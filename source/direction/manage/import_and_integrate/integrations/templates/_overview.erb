GitLab's vision is to be the best [single application for every part of the DevOps toolchain](/handbook/product/single-application/). However, we acknowledge that to achieve this, there are many workflows, custom scripts, and nuanced integrations that customers require and GitLab may not be able to prioritize. To make the most impact towards that vision, we've identified a narrower vision for the Integrations category:

**Driving GitLab to enable [intuitive collaboration with tools our customers rely on](https://about.gitlab.com/handbook/product/gitlab-the-product/#plays-well-with-others) so they can capture the most value possible from the GitLab product and its many features.**

GitLab will have a robust catalog of lovable, feature-complete integrations that serve customers across all key industries and functions that utilize GitLab. Partners will have diverse but straightforward paths for contributing or coordinating integrations that expand the GitLab ecosystem and the larger developer ecosystem in ways that provide tremendous value to the developer community. And we'll do this all in ways that align with GitLab's culture and values, by promoting transparency, focusing on iteration, and incentivizing collaboration/contribution throughout the community.

We will direct our focus to building self-service tools, documentation, guides, best practices, and standards that support our product teams, partners, and contributors to scale the growth of our ecosystem. By positioning our team in the role of a platform team, we believe we can accelerate the process of building and maintaining integrations. We'll also work in a collaboration model to support product teams, help them define their strategy, and execute in a way that best solves for customers and for the business. We'll provide guidance and work to scale that support across product teams so we can achieve our goal of expanding the ecosystem.

A few guidelines for prioritizing integrations across GitLab:
  - We'll have a strong emphasis on partnerships/tools that fill gaps for our product, complement our product, and add value for Enterprise customers. Based on our [Leadership Integrations Survey (Q4 2022)](https://docs.google.com/presentation/d/1ovHeGfEnFAWzQTEYJe6SHIC2cUyn4QjO35-uQmVskSo/edit?usp=sharing), our customers have indicated the most important workflows for integration are CI, AppDev, and Security.
  - As we work to expand our strategic partnerships, we'll prioritize achieving a Lovable state for each integration, with a focus on stability, reliability, performance, and usability, before we prioritize new integrations. This will provide a stable foundation for our ecosystem and allow us to move more quickly on new integrations.

We're inspired by other companies with rich, developer-friendly experiences like [Salesforce](https://developer.salesforce.com/), [Shopify](https://help.shopify.com/en/api/getting-started), [Twilio](https://www.twilio.com/docs/), [Stripe](https://stripe.com/docs/development), and [GitHub](https://developer.github.com/).

A large part of the success of these companies comes from their enthusiasm around enabling developers to integrate, extend, and interact with their services in new and novel ways, creating a spirit of [collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [diversity](https://about.gitlab.com/handbook/values/#diversity-inclusion) that simply can't exist any other way.

#### Shifting integrations within each domain to the relevant GitLab product group as DRI and providing tooling, process, and support to scale

As we continue to scale how we support integrations at GitLab, we'll be working closely with product teams to shift support and prioritization to the relevant areas of the product. Similar to APIs, webhooks, audit events, authentication, and other horizontal services, integrations are better supported by the teams closest to the source of customer pain and need. Rather than centralized prioritization, this enables teams to think outside of the box about how integrations can take their features to the next level, gain more exposure from new audiences, or integrate strategically with competitors to achieve business outcomes.

To support teams in building integrations, we'll be focusing on the technology and tools product teams will leverage to build integrations, including our APIs, webhooks, our Static Integrations DSL, and further abstractions to simplify the process of building integrations.

To achieve this we'll be focusing on the following areas:

* Defining ownership of integrations in GitLab
* Adding all existing integrations to our [Integrations Static DSL](https://gitlab.com/groups/gitlab-org/-/epics/7652)
* Improving our review process with [automations to guide new contributors (internal and external)](https://gitlab.com/gitlab-org/gitlab/-/issues/385969)
* Enhancing our [Integrations Development Guide](https://docs.gitlab.com/ee/development/integrations/) to support more use cases
* Simplifying testing and app submission processes so it's less cumbersome for contributors.